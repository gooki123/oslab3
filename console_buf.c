#include "simulator_lab2.h"
#include "scheduler.h"
#include "console_buf.h"
#include "syscall.h"
#include "memory.h"

void *console_buf_read(){
  char c;
  buf_count = 0;
  while(1){
    //do nothing yet
    P_kt_sem(consoleWait);
    c = console_read();
    if (buf_count < 256) {
      dll_append(read_buff,new_jval_i((int)c));
      buf_count++;
      V_kt_sem(nelem);
    }
  }
}
