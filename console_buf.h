#include "simulator_lab2.h"
#include "dllist.h"
#include "jval.h"
#include "kt.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
/* console buf */
void* console_buf_read();
int buf_count;
Dllist read_buff;
/* end of console buf*/
//semaphore
kt_sem nelem;
kt_sem consoleWait;
kt_sem write_sem;
kt_sem writers;
kt_sem waiter_sem;

