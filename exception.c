/*
 * exception.c -- stub to handle user mode exceptions, including system calls
 *
 * Everything else core dumps.
 *
 * Copyright (c) 1992 The Regents of the University of California. All rights
 * reserved.  See copyright.h for copyright notice and limitation of
 * liability and disclaimer of warranty provisions.
 */
#include "simulator_lab2.h"
#include "scheduler.h"
#include "console_buf.h"
#include "syscall.h"
#include "memory.h"

void
exceptionHandler(ExceptionType which)
{
	int             type, r5, r6, r7, newPC,i;
	int             buf[NumTotalRegs];

	examine_registers(buf);


  type = buf[4];
  r5 = buf[5];
  r6 = buf[6];
  r7 = buf[7];
  newPC = buf[NextPCReg];

  memcpy(currentPCB->registers,buf,sizeof(buf));
	/*
	 * for system calls type is in r4, arg1 is in r5, arg2 is in r6, and
	 * arg3 is in r7 put result in r2 and don't forget to increment the
	 * pc before returning!
	 */

	switch (which) {
	case SyscallException:
		/* the numbers for system calls is in <sys/syscall.h> */
		switch (type) {
		case 0:
			/* 0 is our halt system call number */
			DEBUG('e', "Halt initiated by user program\n");
			SYSHalt();
			break;
		case SYS_exit:
      DEBUG('e', "exit system call\n");
			/* this is the _exit() system call */
      kt_fork(do_exit,(void*)currentPCB);
      //SYSHalt();
			break;
    case SYS_write:
      DEBUG('e', "write system call\n");
    	kt_fork(do_write,(void*)currentPCB);
      break;
    case SYS_read:
      DEBUG('e', "read system call\n");
      kt_fork(do_read,(void*)currentPCB);
      break;
    case SYS_ioctl:
      DEBUG('e', "ioctl system call\n");
      kt_fork(ioctl,(void*)currentPCB);
      break;
    case SYS_fstat:
      DEBUG('e', "fstat system call\n");
      kt_fork(fstat,(void*)currentPCB);
      break;
    case SYS_getpagesize:
      DEBUG('e', "getpagesize system call\n");
      kt_fork(getpagesize,(void*)currentPCB);
      break;
    case SYS_sbrk:
      DEBUG('e', "sbrk system call\n");
      kt_fork(do_sbrk,(void*)currentPCB);
      break;
    case SYS_execve:
      DEBUG('e', "execve system call\n");
      doexec = 1;
      kt_fork(do_execve,(void*)currentPCB);
      break;
    case SYS_getpid:
      DEBUG('e', "getpid system call\n");
      kt_fork(do_getpid,(void*)currentPCB);
      break;
    case SYS_fork:
      DEBUG('e', "fork system call\n");
      kt_fork(do_fork,(void*)currentPCB);
      break;
    case SYS_getdtablesize:
      DEBUG('e', "getdtablesize system call\n");
      kt_fork(do_getdtablesize,(void*)currentPCB);
      break;
    case SYS_getppid:
      DEBUG('e', "getppid system call\n");
      kt_fork(do_getppid,(void*) currentPCB);
      break;
    case SYS_close:
      DEBUG('e', "close system call\n");
      kt_fork(do_close,(void*)currentPCB);
      break;
    case SYS_wait:
      DEBUG('e', "wait system call\n");
      kt_fork(do_wait,(void*)currentPCB);
      break;
    default:
      DEBUG('e', "Unknown system call\n");
      SYSHalt();
      break;
    }
    break;
  case PageFaultException:
    DEBUG('e', "Exception PageFaultException\n");
    break;
  case BusErrorException:
    DEBUG('e', "Exception BusErrorException\n");
    break;
  case AddressErrorException:
    //printf("currentPCB base: %i\n", currentPCB->base);
    //printf("memory size: %i\n", MemorySize);
    DEBUG('e', "Exception AddressErrorException\n");
    break;
  case OverflowException:
    DEBUG('e', "Exception OverflowException\n");
    break;
  case IllegalInstrException:
    DEBUG('e', "Exception IllegalInstrException\n");
    break;
  default:
    printf("Unexpected user mode exception %d %d\n", which, type);
    exit(1);
    break;
  }
  kt_joinall();
  schedulePCB();
}

void
interruptHandler(IntType which)
{
	int buf[NumTotalRegs],i;
  Jval val;
	//examine_registers(buf);
  //memcpy(currentPCB->registers,buf,NumTotalRegs);
	switch (which) {
	case ConsoleReadInt:
		DEBUG('e', "ConsoleReadInt interrupt\n");
    if (noopflag != 1)
    {
      examine_registers(currentPCB->registers);
	    dll_prepend(readyq,new_jval_v(currentPCB));
    }
    V_kt_sem(consoleWait);
		break;
	case ConsoleWriteInt:
		DEBUG('e', "ConsoleWriteInt interrupt\n");
    if (noopflag != 1) {
      examine_registers(currentPCB->registers);
  	  dll_prepend(readyq,new_jval_v(currentPCB));
    }
    V_kt_sem(write_sem);
		break;
  case TimerInt:
    // DEBUG('e', "TimerInt interrupt\n");
    if (noopflag != 1) {
      examine_registers(currentPCB->registers);
      dll_prepend(readyq,new_jval_v(currentPCB)); 
    }
    break;
	default:
		DEBUG('e', "Unknown interrupt\n");
		break;
	}
  kt_joinall();
	schedulePCB();
}
