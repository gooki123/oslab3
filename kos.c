/*
 * jos.c -- starting point for student's os.
 *
 */
#include "simulator_lab2.h"
#include "scheduler.h"
#include "console_buf.h"
#include "syscall.h"
#include "memory.h"
#include "time.h"
#define KOS_C

/* semaphores */
kt_sem nelem;
kt_sem consoleWait;
kt_sem write_sem;
kt_sem writers;
JRB pids;
/* end of semaphores */

void perform_execve(PCB* job, char* fn, void* argV ){
  job->base = User_Base;
  job->limit = User_Limit;
  job->registers[PCReg]=0;
  job->registers[NextPCReg]=4;
  int i;
  char **kosargv = argV;
  job->sbrk = load_user_program(fn);
  if (job->sbrk < 0 || job->sbrk > job->limit) {
    fprintf(stderr,"Can't load program.\n");
    do_exit((void*)job);
  }

  /* need to back off from top of memory */
  /* 12 for argc, argv, envp */
  /* 12 for stack frame */
  int tos = MemorySize/8 - 20;

  /* number of arguments you put in */
  int argc = 0;
  while (kosargv[argc++] != NULL);
  argc--;
  /* end of part */

  int arg_counter = 0;
  int arg_addr[argc];

  for(arg_counter; arg_counter < argc; arg_counter++) {
      tos -= (strlen(kosargv[arg_counter])+1);
      arg_addr[arg_counter] = tos;
      strcpy(job->base+main_memory+tos,kosargv[arg_counter]);
  }

  while (tos%4) tos--;

  tos -= 4;
  arg_counter = argc;
  int k = WordToMachine(0);
  memcpy(job->base+main_memory+tos,&k,4);
  for (arg_counter; arg_counter >= 0; arg_counter--) {
    tos-=4;
    k = WordToMachine(arg_addr[arg_counter]);
    memcpy(job->base+main_memory+tos,&k,4);
  }

  int argv = tos;

  tos -= 4;
  k = WordToMachine(0);
  memcpy(job->base+main_memory+tos, &k, 4);
  tos -= 4;
  k = WordToMachine(argv);
  memcpy(job->base+main_memory+tos,&k,4);

  tos -= 4;
  k = WordToMachine(argc);
  memcpy(job->base+main_memory+tos, &k, 4);
  //printf("stackreg:%i\n",job->registers[StackReg]);
  //if (doexec == 1)
  //  job->registers[StackReg] = tos + 12;
  //else
  job->registers[StackReg] = tos - 12;
  doexec = 0;
  eqPCB(job);
  kt_exit();
}
void* initialize_user_process(void *args) {
  User_Base = 0;
  User_Limit = MemorySize/8;
  currentPCB = (PCB*)malloc(sizeof(PCB));
  currentPCB->waiter_sem = make_kt_sem(0);
  currentPCB->waiters = new_dllist();
  currentPCB->children = make_jrb();
  currentPCB->pid = get_new_pid();
  currentPCB->base = 0;
  currentPCB->limit = MemorySize/8;
  forkArray[0] = 1;
  read_buff = new_dllist();
  int i;
  initPCB = (PCB*)malloc(sizeof(PCB));
  currentPCB->parent = initPCB;
  currentPCB->parent->pid = 0;
  currentPCB->parent->waiter_sem = make_kt_sem(0);
  currentPCB->parent->waiters = new_dllist();
  initPCB->children = make_jrb();
  jrb_insert_int(initPCB->children,currentPCB->pid,new_jval_v(currentPCB));
  /* reset regs here */
  for (i=0; i < NumTotalRegs; i++)
    currentPCB->registers[i] = 0;

  /* set up the program counters and the stack register */
  currentPCB->registers[PCReg] = 0;
  currentPCB->registers[NextPCReg] = 4;

  bzero(main_memory, MemorySize);
  perform_execve(currentPCB,kos_argv[0],kos_argv);
//    syscall_return(currentPCB, err);
  syscall_return(currentPCB,0);
}


KOS() {
  /* arg stuff */
  int i;
  for(i = 0; i < 8; i ++){
    forkArray[i] = 0;
  }
  doexec = 0;
  curpid = 0;
  noopflag = 0;
  pids = make_jrb();
  consoleWait = make_kt_sem(0);
  nelem = make_kt_sem(0);
  write_sem = make_kt_sem(0);
  writers = make_kt_sem(1);
  kt_fork(initialize_user_process, kos_argv);
  kt_fork(console_buf_read, NULL);
  kt_joinall();
  start_timer(10);
  schedulePCB();
}

