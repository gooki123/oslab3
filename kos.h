#ifndef	KOS_C
#define EXTERN
#else
#define EXTERN extern
#endif

#include "simulator_lab2.h"
#include "dllist.h"
#include "jval.h"
#include "kt.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

/*scheduler stuff */
typedef struct PCB {
  int registers[NumTotalRegs];
} PCB;
EXTERN int noopflag;
EXTERN Dllist readyq;
EXTERN PCB* currentPCB;
EXTERN void eqPCB(PCB *cPCB);
EXTERN PCB* dqPCB();
EXTERN void schedulePCB();

/*end of scheduler stuff*/

// syscall
EXTERN void* do_write(void*);
EXTERN void* do_read(void*);
// end of syscall

/* semaphores */
EXTERN kt_sem nelem;
EXTERN kt_sem consoleWait;
EXTERN kt_sem write_sem;
EXTERN kt_sem writers;
/* end of semaphores */

/* console buf */
EXTERN void* console_buf_read();
EXTERN int buf_count;
EXTERN Dllist read_buff;
/* end of console buf*/
