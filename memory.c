#include "memory.h"
#include "scheduler.h"

int get_new_pid(){
  int found;
  curpid = 1;
  if (jrb_empty(pids) == 1) {
    jrb_insert_int(pids,curpid,new_jval_v(currentPCB));
    return curpid;
  }
  while(jrb_find_int(pids,curpid)!=NULL){
    curpid++;
  }
  jrb_insert_int(pids,curpid,new_jval_v(currentPCB));
  return curpid;
}

void destroy_pid(int pid){
  JRB item = jrb_find_int(pids,pid);
  if(item != NULL)
    jrb_delete_node(item);
}
