#ifndef MEMORY_C
#define MEMORY_C

#include "simulator_lab2.h"
#include "dllist.h"
#include "jval.h"
#include "jrb.h"
int curpid;
int get_new_pid();
extern JRB pids;
void destroy_pid(int);
#endif
