#include "scheduler.h" 
#include "simulator_lab2.h"
void eqPCB(PCB *cPCB) {
  //printf("cPCB pid: %i\n", cPCB->pid);
  if (readyq == NULL) {
    readyq = new_dllist();
    dll_prepend(readyq,new_jval_v(cPCB));
  }
  else
  {
    dll_prepend(readyq,new_jval_v(cPCB));
  }
}

PCB* dqPCB() {
  Jval item= ((Jval)dll_val(dll_last(readyq)));
  dll_delete_node(dll_last(readyq));
  return (PCB*)item.v;
}

void schedulePCB() {
  if (jrb_empty(initPCB->children)) {
    SYSHalt();
  }
  if (dll_empty(readyq)==1) {
    noopflag = 1;
    noop();
  }
  else
  {
    noopflag = 0;
    PCB *cPCB = dqPCB();
    currentPCB = cPCB;
    User_Base = cPCB->base;
    User_Limit = cPCB->limit;
    run_user_code(cPCB->registers);
  }
}

