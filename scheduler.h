#ifndef SCHEDULER_C
#define SCHEDULER_C
#include "simulator_lab2.h"
#include "dllist.h"
#include "jval.h"
#include "jrb.h"
#include "kt.h"
/*scheduler stuff */
typedef struct PCB {
  unsigned short pid;
  struct PCB *parent;
  Dllist waiters;
  kt_sem waiter_sem;
  JRB children; 
  int registers[NumTotalRegs];
  int base;
  int limit;
  int sbrk;
  int exitval;
} PCB;
int noopflag;
Dllist readyq;
PCB* currentPCB;
PCB* initPCB;
void eqPCB(PCB *cPCB);
PCB* dqPCB();
void schedulePCB();
/*end of scheduler stuff*/

#endif
