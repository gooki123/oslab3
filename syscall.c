#include "simulator_lab2.h"
#include "scheduler.h"
#include "console_buf.h"
#include "syscall.h"
#include "memory.h"

int ValidAddress(PCB *cPCB,int mem) {
  int high = cPCB->limit;
  if(mem >= 0 && mem < high){
    return 1;
  }
  return 0;
}
void syscall_returne(PCB *cPCB, int value) {
  cPCB->registers[PCReg]=0;
  cPCB->registers[NextPCReg]=4;
  dll_prepend(readyq,new_jval_v(cPCB));
  kt_exit();
}
void syscall_return(PCB *cPCB, int value){
  cPCB->registers[PCReg]= cPCB->registers[NextPCReg];
  cPCB->registers[2] = value;
  dll_prepend(readyq,new_jval_v(cPCB));
  kt_exit();
}
void* finish_fork(void *args) {
  PCB* cPCB = (PCB*) args;
  syscall_return(cPCB, 0);
}
void* do_write(void* args){
  PCB *cPCB = (PCB*) args;
  int count = 0;
  int str_len = cPCB->registers[7];
  int mem_addr = cPCB->registers[6];
  if(ValidAddress(cPCB,mem_addr) == 0){
    syscall_return(cPCB,-EFAULT);
  }
  if (cPCB->registers[6] < 0 )
  {
    syscall_return(cPCB, -EFAULT);
  }
  else if (cPCB->registers[5] != 1 && cPCB->registers[5] != 2) {
    syscall_return(cPCB, -EBADF);
  }
  P_kt_sem(writers);
  while (count < str_len) {
    console_write(main_memory[cPCB->base+mem_addr+count++]);
    P_kt_sem(write_sem);
  }
  V_kt_sem(writers);
  syscall_return(cPCB, count);
}

void* do_read(void* args){
  PCB *cPCB = (PCB *) args;
  Jval val;
  int str_len = cPCB->registers[7];
  int mem_addr = cPCB->registers[6];
  if(ValidAddress(cPCB,mem_addr) == 0){
    syscall_return(cPCB,-EFAULT);
  }
  if (cPCB->registers[7] < 0) {
    syscall_return(cPCB,-EINVAL);
  }
  else if (cPCB->registers[6] < 0 || cPCB->registers[6]%4!=0)
  {
    syscall_return(cPCB, -EFAULT);
  }
  else if (cPCB->registers[5]!=0)
  {
    syscall_return(cPCB, -EBADF);
  }

  int mem_count = 0;
  while (mem_count != str_len)
  {
    P_kt_sem(nelem);
    if (dll_val(dll_first(read_buff)).i==-1){
      dll_delete_node(dll_first(read_buff));
      break;
    }
    main_memory[cPCB->base+mem_addr+mem_count] = (char)dll_val(dll_first(read_buff)).i;
    if((int)dll_val(dll_first(read_buff)).i == 10){
      dll_delete_node(dll_first(read_buff));
      mem_count++;
      break;
    }
    dll_delete_node(dll_first(read_buff));
    mem_count++;
    if (mem_count == str_len)
    {
      break;
    }
  }
  syscall_return(cPCB,mem_count);
}

void* ioctl(void *args) {
  PCB *cPCB = (PCB *) args;
  int arg1 = cPCB->registers[5];
  int arg2 = cPCB->registers[6];
  char* x = cPCB->base+main_memory+cPCB->registers[7];
  if(ValidAddress(cPCB,cPCB->registers[7]) == 0){
    syscall_return(cPCB,-EFAULT);
  }

  if (arg1 != 1 || arg2 != JOS_TCGETP) {
    syscall_return(cPCB,-EINVAL);
  }
  ioctl_console_fill(x);
  syscall_return(cPCB,0);
}

void* fstat(void* args){
  PCB *cPCB = (PCB *) args;
  int fd = cPCB->registers[5];
  int bk_size_1 = 1;
  int bk_size_256 = 256;
  char* x = cPCB->base+main_memory+cPCB->registers[6];
  if(ValidAddress(cPCB,cPCB->registers[6]) == 0){
    syscall_return(cPCB,-EFAULT);
  }
  if(fd == 0){
    stat_buf_fill(x,bk_size_1);
  }
  else if (fd == 1 || fd == 2){
    stat_buf_fill(x,bk_size_256);
  }
  syscall_return(cPCB,0);
}
void* getpagesize(void* args) {
  PCB* cPCB = (PCB*) args;
  syscall_return(cPCB,PageSize);
}

void* do_sbrk(void* args){
  PCB *cPCB = (PCB *) args;
  int addr = cPCB->registers[5];
  if(ValidAddress(cPCB,addr) == 0){
    syscall_return(cPCB,-EFAULT);
  }
  int oldsbrk = cPCB -> sbrk;
  cPCB->sbrk += addr;
  syscall_return(cPCB,oldsbrk);
}

void* do_execve(void* args){
  PCB *cPCB = (PCB*) args;
  int strLength;
  int k = 0;
  int arg0 = cPCB->registers[5];
  int argv = cPCB->registers[6];
  int p = 1;
  int count = 0;
  while(p!=0) {
    count++;
    memcpy(&p,cPCB->base+main_memory+argv+k,4);
    k+=4;
  }
  strLength = strlen((char *)(cPCB->base+main_memory+arg0))+1;
  file_name = (char *)malloc(strLength);
  memcpy(file_name,cPCB->base+main_memory+arg0,strLength);
  if (strLength==0)
    syscall_return(cPCB,-ENOENT);
  char **temp_MEM = (char**)malloc(count*sizeof(char*));
  temp_MEM[count-1]=NULL;
  int i = 0;
  k = 0;
  for (i; i < count-1; i++) {
    memcpy(&p,cPCB->base+main_memory+argv+k,4);
    strLength = strlen((char *)(cPCB->base+main_memory+p))+1;
    temp_MEM[i] = (char *)malloc(strLength);
    memcpy(temp_MEM[i],cPCB->base+p+main_memory,strLength);
    k+=4;
  }
  for (i=0; i < NumTotalRegs; i++)
    currentPCB->registers[i] = 0;
  cPCB->registers[PCReg]=0;
  cPCB->registers[NextPCReg]=4;
  perform_execve(cPCB,file_name,temp_MEM);
  free(temp_MEM);
  syscall_return(cPCB,0);
}

void* do_getpid(void *args) {
  PCB* cPCB = (PCB*) args;
  syscall_return(cPCB,cPCB->pid);
}

void* do_fork(void *args) {
  PCB* cPCB = (PCB*) args;
  int i,currentIndex;
  for(i = 0; i < 8; i ++){
    if(forkArray[i]==0){
      currentIndex = i;
      forkArray[i] = 1;
      break;
    }
    if(i == 7 && forkArray[i] == 1){
      syscall_return(cPCB,-EAGAIN);
    }
  }
  PCB* newPCB = (PCB *) malloc(sizeof(PCB));
  newPCB->pid = get_new_pid();
  newPCB -> parent = currentPCB;
  newPCB -> base = currentIndex * (MemorySize/8);
  newPCB -> limit = (MemorySize/8);
  newPCB -> sbrk = cPCB->sbrk;
  newPCB -> waiters = new_dllist();
  newPCB -> waiter_sem = make_kt_sem(0);
  newPCB -> children = make_jrb();
  jrb_insert_int(cPCB->children,newPCB->pid,new_jval_v(newPCB));
  JRB iterator;
  memcpy(main_memory+newPCB->base, main_memory+cPCB->base,cPCB->limit );
  for (i=0; i < NumTotalRegs; i++)
    newPCB->registers[i] = cPCB->registers[i];
  kt_fork(finish_fork,newPCB);
  syscall_return(cPCB,newPCB->pid);
}

void* do_exit(void *args) {
  PCB* cPCB = (PCB*) args;
  int i = cPCB->base/(MemorySize/8);
  forkArray[i] = 0;
  cPCB->exitval = cPCB->registers[5];



  JRB iterator;
  while(!dll_empty(cPCB->waiters)) {
    PCB* temp = (PCB*)dll_val(dll_first(cPCB->waiters)).v;
    dll_delete_node(dll_first(cPCB->waiters));
    destroy_pid(temp->pid);
  }
  if (cPCB->parent->pid != initPCB->pid){
    dll_prepend(cPCB->parent->waiters,new_jval_v(cPCB));
    V_kt_sem(cPCB->parent->waiter_sem);
    jrb_traverse(iterator,cPCB->children) {
      PCB* tempPCB = ((PCB*)(((Jval)(iterator->val)).v));
      tempPCB->parent = initPCB;
      jrb_insert_int(cPCB->parent->children,tempPCB->pid,new_jval_v(tempPCB));
    }
  }
  else {
    int mypid = cPCB->pid;
    JRB tobedeleted = jrb_find_int(initPCB->children,mypid);
    if (tobedeleted != NULL)
      jrb_delete_node(tobedeleted);
    destroy_pid(mypid);
  }
  kt_exit();
}
 void* do_getdtablesize(void* args) {
  PCB* cPCB = (PCB*) args;
  syscall_return(cPCB,64);
}

void* do_getppid(void* args){
  PCB* cPCB = (PCB*) args;
  syscall_return(cPCB,cPCB->parent->pid);
}

void* do_close(void* args) {
  PCB* cPCB = (PCB*) args;
  syscall_return(cPCB,-EBADF);
}

void* do_wait(void* args) {
  PCB* cPCB = (PCB*) args;
  P_kt_sem(cPCB->waiter_sem);
  int status = cPCB->registers[5];
  PCB *waiterPCB =((PCB*)(((Jval)dll_val(dll_last(cPCB->waiters))).v));
  int pid = waiterPCB->pid;
  int exitval = waiterPCB->exitval;
  // memcpy(main_memory+cPCB->base+status,&waiterPCB->exitval,sizeof(int));
  destroy_pid(pid);
  dll_delete_node(dll_last(cPCB->waiters));
  //free((PCB*)(((Jval)dll_val(dll_last(cPCB->waiters))).v));
  syscall_return(cPCB,pid);
}
