#include "jrb.hcPCB->pid"
#include "jval.h"
#include <stdio.h>

int main() {
	JRB myjrb = make_jrb();
	JRB iterator;
	int found = 0;
	jrb_insert_int(myjrb,0,new_jval_i(0));
	jrb_insert_int(myjrb,1,new_jval_i(0));
	jrb_insert_int(myjrb,2,new_jval_i(0));
	jrb_insert_int(myjrb,3,new_jval_i(0));
	jrb_insert_int(myjrb,4,new_jval_i(0));
	jrb_traverse(iterator,myjrb) {
		printf("myjrb key: %i\n", (iterator->key).i);
		jrb_delete_node(iterator);
	}
	if (jrb_empty(myjrb))
		printf("yay empty!!!\n");
	/*
	while(!jrb_empty(myjrb)) {
		JRB tobedeleted = jrb_find_gte_int(myjrb,0,&found);
		if (tobedeleted!=NULL) {
			printf("myjrb key: %i\n", (tobedeleted->key).i);
			jrb_delete_node(tobedeleted);
		}
		myjrb->blink = jrb_next(myjrb);
	}
	*/
}